
// Exam 1 Practical
// Justin Rankin

#include <iostream>
#include <conio.h>

using namespace std;

float Cube(float);
float Square(float);

int main()
{
	float usernum;
	char contin;
	char incriment;


	do {
		cout << "Please enter the number you would like to square or cube" << "\n" ;
		cin >> usernum;
		cout << "\n" << "If you would like to cube a function please enter '3'" << "\n" << "If you would like to square a function please enter '2' " << "\n" << "\n";
		cin >> incriment;

		switch (incriment){
		case '2':
			cout << "\n" << "Your Answer is " << Square(usernum) << "\n" << "\n";
			break;
		case'3':
			cout << "\n" << "Your answer is " << Cube(usernum) << "\n" << "\n";
			break;
		}

		cout << "Would you like to perform another function? (Y/N)" << "\n" << "\n";
		cin >> contin;
	} while (contin == 'Y');


	(void)_getch();
	return 0;
}


float Square(float num1) {
	float answer = num1 * num1;
	return answer;
}

float Cube(float num1) {
	float answer = num1 * num1 * num1;
	return answer;
}